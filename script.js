"use strict";

function request(url, method) {
  return fetch(url, {
    method: method,
  }).then((response) => {
    if (method === "DELETE") {
      return response;
    } else {
      return response.json();
    }
  });
}
function loadingAnimationToggle(parentNode, remove) {
  if (remove) {
    parentNode.querySelector(".loading").remove();
  } else {
    parentNode.insertAdjacentHTML(
      "beforeend",
      "<div class='loading'><div class='loading-animation'></div> Loading...</div>"
    );
  }
}
function createDOMElement(tag, classList, id) {
  const element = document.createElement(tag);
  if (classList) element.classList.add(...classList);
  if (id) element.id = id;
  return element;
}

class Card {
  constructor(user, userPosts) {
    this.user = user;
    this.userPosts = userPosts;
  }
  getCardInfoForRender() {
    return {
      name: this.user.name,
      email: this.user.email,
      companyName: this.user.company.name,
      posts: this.userPosts.map((post) => {
        const { title, body, id } = post;
        return { title, body, id };
      }),
    };
  }
  cardRemover(id) {
    request(`https://ajax.test-danit.com/api/json/posts/${id}`, "DELETE").then(
      (response) => {
        if (response.ok) {
          document.getElementById(id).remove();
        }
      }
    );
  }
  render() {
    const cardInfo = this.getCardInfoForRender();
    return cardInfo.posts.map((post) => {
      const newCard = createDOMElement("div", ["card"], post.id);
      newCard.innerHTML = `<div>
        <h2>${cardInfo.name}<span class="card__author-username">${cardInfo.email}</span></h2>
        <h3 class="card__company">${cardInfo.companyName}</h3>
        <p class="card__title">${post.title}</p>
        <p>${post.body}</p>
      </div>`;
      const deleteBtn = createDOMElement("button", ["delete"]);
      deleteBtn.innerHTML = `<svg viewBox="0 0 448 512"><path
        d="M135.2 17.7C140.6 6.8 151.7 0 163.8 0H284.2c12.1 0 23.2 6.8 28.6 17.7L320 32h96c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 96 0 81.7 0 64S14.3 32 32 32h96l7.2-14.3zM32 128H416V448c0 35.3-28.7 64-64 64H96c-35.3 0-64-28.7-64-64V128zm96 64c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16zm96 0c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16zm96 0c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16z" /></svg>`;
      deleteBtn.addEventListener("click", (event) => {
        event.preventDefault();
        this.cardRemover(event.target.closest("div").id);
      });
      newCard.append(deleteBtn);
      return newCard;
    });
  }
}

const usersUrl = `https://ajax.test-danit.com/api/json/users`;
const postsUrl = `https://ajax.test-danit.com/api/json/posts`;

loadingAnimationToggle(document.body, false);
Promise.all([request(usersUrl, "GET"), request(postsUrl, "GET")])
  .then((response) => {
    const users = response[0];
    const posts = response[1];
    users.forEach((user) => {
      const usersPosts = posts.filter((post) => user.id === post.userId);
      const card = new Card(user, usersPosts);
      document.body.append(...card.render());
    });
    loadingAnimationToggle(document.body, true);
  })
  .catch((err) => console.log(err));
